package com.example.mlsr.finalbarcodereaderapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import java.util.ArrayList;
import java.util.List;
import info.androidhive.barcode.BarcodeReader;

public class barCodeNewActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener{

    ArrayList<String> barCodesScannedData;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code_new);
        barCodesScannedData = new ArrayList<String>();
        barCodesScannedData.add("scanned data");

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Bar Code Scanner");
        setSupportActionBar(toolbar);
        Toast.makeText(getApplicationContext(),"scanning started",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onScanned(Barcode barcode) {

        String result ="hi";
        int i=0;
        while(i < barCodesScannedData.size()) {
            if ( barCodesScannedData.get(i).equals(barcode.displayValue)) {
                break;
            }
            else{
                ++i;
            }
        }
        if ( i == barCodesScannedData.size()){
            barCodesScannedData.add(barcode.displayValue);
        }

        String op = "";
        for( String e: barCodesScannedData) {
            op += e + " " +"\n";
        }
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_back) {
            Intent i = new Intent(getApplicationContext(),LoginActivity.class);
            i.putStringArrayListExtra("data",barCodesScannedData);
            startActivity(i);
            Toast.makeText(getApplicationContext(),"scanning has been stopped", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
