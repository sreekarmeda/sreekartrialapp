package com.example.mlsr.finalbarcodereaderapplication;
import android.content.Context;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;

import android.graphics.Matrix;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private Button button1;
    private TextView textView;
    Bitmap myBitmap;
    private Context context;
    Bitmap converetdImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView);
        button1 = (Button) findViewById(R.id.Scan_Button);
        textView = (TextView) findViewById(R.id.textView);

        myBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.qrcodeimage);
        converetdImage = getResizedBitmap(myBitmap, 200);
        imageView.setImageBitmap(converetdImage);


        scanButtonClick();
    }

    public void scanButtonClick() {

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BarcodeDetector detector = new BarcodeDetector.Builder(getApplicationContext())
                        .setBarcodeFormats(Barcode.ALL_FORMATS)
                        .build();



                Frame frameData = new Frame.Builder().setBitmap(converetdImage).build();

                SparseArray<Barcode> barcodes = detector.detect(frameData);

                String result = "Hello";
                if (barcodes.size() != 0){
                    result = barcodes.valueAt(0).rawValue;
                }
                textView.setText(result);
            }
        });
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
