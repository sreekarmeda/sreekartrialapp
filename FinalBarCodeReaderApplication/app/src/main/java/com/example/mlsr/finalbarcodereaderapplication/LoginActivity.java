package com.example.mlsr.finalbarcodereaderapplication;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private Button ScanButton;
    private TextView textView;
    ArrayList<String> scannedInformation;
    private Toolbar tool_bar_loginactivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ScanButton = (Button) findViewById(R.id.button_scan);
        textView = (TextView) findViewById(R.id.TextViewID);
        scannedInformation = new ArrayList<String>();

        tool_bar_loginactivity = (Toolbar) findViewById(R.id.tool_bar_login_activity);
        setSupportActionBar(tool_bar_loginactivity);


        ScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),barCodeNewActivity.class);
                startActivity(i);
                finish();

            }
        });

        scannedInformation = getIntent().getStringArrayListExtra("data");

        String op = " ";
        try {
            for( String e: scannedInformation) {
                op += e + " " +"\n";
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        textView.setText(op);
    }

    @Override
    public void onBackPressed() {
        return ;
    }
}
