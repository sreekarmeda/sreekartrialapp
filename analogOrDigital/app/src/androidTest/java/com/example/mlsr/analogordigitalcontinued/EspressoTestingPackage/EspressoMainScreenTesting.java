package com.example.mlsr.analogordigitalcontinued.EspressoTestingPackage;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.mlsr.analogordigitalcontinued.AnroidEspressoAutomationTesting.EspressoMainActivity;
import com.example.mlsr.analogordigitalcontinued.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by MlSr on 2/15/2018.
 */

@RunWith(AndroidJUnit4.class)
public class EspressoMainScreenTesting {

    @Rule
    public ActivityTestRule<EspressoMainActivity> mActivityRule =
            new ActivityTestRule<>(EspressoMainActivity.class);

    @Test
    public void ensureTextChangesWork() {
        // Type text and then press the button.


        onView(withId(R.id.inputField))
                .perform(typeText("HELLO"), closeSoftKeyboard());

        onView(withId(R.id.changeText)).perform(click());

        // Check that the text was changed.
        onView(withId(R.id.inputField)).check(matches(withText("Hello world")));

    }
}
