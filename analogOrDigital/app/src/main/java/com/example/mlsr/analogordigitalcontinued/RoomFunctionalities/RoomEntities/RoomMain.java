package com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mlsr.analogordigitalcontinued.R;
import com.example.mlsr.analogordigitalcontinued.RoomDB.AppDatabase;

import java.util.List;

public class RoomMain extends AppCompatActivity {

    private EditText UserNameEditText;
    private EditText PasswordEditText;
    private EditText RoleEditText;
    private Button LoginButton;
    private AppDatabase appDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_main);

        appDatabase = AppDatabase.getDatabase(this.getApplication());


        UserNameEditText =(EditText) findViewById(R.id.username);
        PasswordEditText =(EditText) findViewById(R.id.password);
        RoleEditText = (EditText) findViewById(R.id.role_name);
        LoginButton = (Button) findViewById(R.id.LoginButton);

        addingDataintoEmployeeLoginDetails();

      //  addManualDataintoEmployeeRoles();
    }
   // RegularEmployee

    public void addingDataintoEmployeeLoginDetails() {
        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             //   adding employee Login details
                EmployeeLoginDetails employeeLoginDetails = new EmployeeLoginDetails(UserNameEditText.getText().toString(),PasswordEditText.getText().toString(),RoleEditText.getText().toString());
                appDatabase.userDao().addEmployeeLoginDetails(employeeLoginDetails);

                //retriving login employee details
                List<EmployeeLoginDetails> employeeLoginDetailsList =  appDatabase.userDao().getAllEmployeeLoginDetails();
                for(int i=0 ;i < employeeLoginDetailsList.size();i++){
                    Log.i("employeeLoginDetails", employeeLoginDetailsList.get(i).getUserName().toString());
                }
            }
        });

    }

    public void addManualDataintoEmployeeRoles(){

        //adding the employee Roles data.
        EmployeeRoles employeeRoles = new EmployeeRoles(1,"Admin");
        appDatabase.userDao().addEmployeeRoles(employeeRoles);
        Log.i("employeeRolesInserted","inserted admin record");

        //retriving the data from Employees Roles.
        List<EmployeeRoles> employeeRolesList = appDatabase.userDao().getAllEmployeeRoles();
        Log.i("EmployeeData",employeeRolesList.get(0).getRoleName().toString());

    }
}
