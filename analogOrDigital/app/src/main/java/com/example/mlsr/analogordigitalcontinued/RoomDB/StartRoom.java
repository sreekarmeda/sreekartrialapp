package com.example.mlsr.analogordigitalcontinued.RoomDB;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mlsr.analogordigitalcontinued.R;

import java.util.List;

public class StartRoom extends AppCompatActivity {

    private EditText UserName;
    private Button addButton;
    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_room);

        UserName = (EditText) findViewById(R.id.DbName);
        addButton = (Button) findViewById(R.id.AddData);

        appDatabase = AppDatabase.getDatabase(this.getApplication());
        List<UserModel> userdata = appDatabase.userDao().getUser();

        for (int i=0;i <= userdata.size();i++) {

            Log.v("name", userdata.get(i).getName().toString());

        }
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserModel userModel = new UserModel(UserName.getText().toString());
                appDatabase.userDao().addData(userModel);
            }
        });
    }
}
