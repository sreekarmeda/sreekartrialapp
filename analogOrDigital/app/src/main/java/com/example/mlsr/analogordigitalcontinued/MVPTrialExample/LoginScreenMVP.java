package com.example.mlsr.analogordigitalcontinued.MVPTrialExample;

/**
 * Created by MlSr on 2/1/2018.
 */

public interface LoginScreenMVP {
      interface View {

        String getUserName();
        String getPassWord();

        void setUserName(String userName);
        void setPassWord(String passWord);

    }

    interface Presenter {

        void getTestData();

        User getCurrentUser();

    }

    interface Model {

        User getUser();
        void createUser(String FirstName,String LastName);

    }
}
