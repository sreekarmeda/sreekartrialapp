package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MlSr on 1/17/2018.
 */

public class MyApiRetrofitModel {
    @SerializedName("Name")
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
