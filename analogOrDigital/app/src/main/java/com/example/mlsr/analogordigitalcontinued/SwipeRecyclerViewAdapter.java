package com.example.mlsr.analogordigitalcontinued;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.mlsr.analogordigitalcontinued.RetrofitModel;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by MlSr on 1/2/2018.
 */

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SimpleViewHolder> {



    private Context mContext;
    private ArrayList<Student> studentList;
    private ArrayList<FriendAndBestFriend> friendsData;
    private ArrayList<RetrofitModel> retrofitModelArrayList;


//    public SwipeRecyclerViewAdapter(Context context, ArrayList<Student> objects) {
//        this.mContext = context;
//        this.studentList = objects;
//    }

    public SwipeRecyclerViewAdapter(Context context, ArrayList<FriendAndBestFriend> objects) {
        this.mContext = context;
        this.friendsData = objects;
    }


//public SwipeRecyclerViewAdapter(Context context, ArrayList<RetrofitModel> arrayListModel) {
//    this.mContext = context;
//    this.retrofitModelArrayList = arrayListModel;
//}



    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_swipe, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder,  final int position) {
      //  final Student item = studentList.get(position);
//        final RetrofitModel item = retrofitModelArrayList.get(position);
        final FriendAndBestFriend item = friendsData.get(position);

        viewHolder.tvName.setText(item.getFriendName() + "  -  Row Position " + position);
        Log.i("name",item.getBestFriendName().toString());
       // viewHolder.tvEmailId.setText(item.getEmailId());
        viewHolder.tvEmailId.setText(item.getBestFriendName());
        viewHolder.teamName.setText(item.getFriendsTeam());


//        OkHttpClient client = new OkHttpClient();
//        client.protocols().add(Protocol.HTTP_2);
//       // client.setProtocols(Arrays.asList(Protocol.HTTP_2));
//        Picasso picasso = new Picasso.Builder(mContext)
//                .downloader((Downloader) client)
//                .build();
//        picasso.load(item.getFriendsPhoto()).into(viewHolder.friendsImage);
  //     Picasso.with(mContext).load(item.getFriendsPhoto()).into(viewHolder.friendsImage);

        Picasso.with(mContext).load(item.getFriendsPhoto()).into(viewHolder.friendsImage);
        Log.v("image",item.getFriendsPhoto());




        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper1));

        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));


        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }
            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        /*viewHolder.swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ((((SwipeLayout) v).getOpenStatus() == SwipeLayout.Status.Close)) {
                    //Start your activity

                    Toast.makeText(mContext, " onClick : " + item.getName() + " \n" + item.getEmailId(), Toast.LENGTH_SHORT).show();
                }

            }
        });*/

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, " onClick : " + item.getFriendName() + " \n" + item.getBestFriendName(), Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext(), "Clicked on Map " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });


        viewHolder.tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(view.getContext(), "Clicked on Share " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(view.getContext(), "Clicked on Edit  " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });


        viewHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                friendsData.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, friendsData.size());
                mItemManger.closeAllItems();
                Toast.makeText(view.getContext(), "Deleted " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return friendsData.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }
//  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        TextView tvName;
        TextView tvEmailId;
        TextView tvDelete;
        TextView tvEdit;
        TextView tvShare;
        TextView teamName;
        ImageButton btnLocation;
        ImageView friendsImage;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvEmailId = (TextView) itemView.findViewById(R.id.tvEmailId);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDelete);
            tvEdit = (TextView) itemView.findViewById(R.id.tvEdit);
            tvShare = (TextView) itemView.findViewById(R.id.tvShare);
            btnLocation = (ImageButton) itemView.findViewById(R.id.btnLocation);
            teamName = (TextView) itemView.findViewById(R.id.team_name);
            friendsImage = (ImageView) itemView.findViewById(R.id.friendsImage);
        }
    }
}