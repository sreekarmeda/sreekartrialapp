package com.example.mlsr.analogordigitalcontinued;

import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.daimajia.swipe.util.Attributes;
import com.example.mlsr.analogordigitalcontinued.RoomDB.AppDatabase;
import com.example.mlsr.analogordigitalcontinued.RoomDB.SuperHerosData;
import com.example.mlsr.analogordigitalcontinued.RoomDB.SuperHerosModel;
import com.example.mlsr.analogordigitalcontinued.RoomDB.UserModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Student> mDataSet;
    private AppDatabase appDatabase;

    ArrayList<FriendAndBestFriend> friendsData1;

    private Toolbar toolbar;
    ArrayList<RetrofitModel> arrayListModel;

    private TextView tvEmptyView;
    private RecyclerView mRecyclerView;
    private RetrofitApiInterface apiInterface;
    int sizeValue = 0;
    SwipeRecyclerViewAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        appDatabase = AppDatabase.getDatabase(this.getApplication());



        // Layout Managers:


        // Item Decorator:
       // mRecyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        // mRecyclerView.setItemAnimator(new FadeInLeftAnimator());
        mDataSet = new ArrayList<Student>();
        arrayListModel = new ArrayList<RetrofitModel>();


//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//            getSupportActionBar().setTitle("Android Students");
//
//        }

        prepareDataSourceRecylerViewAdapter();

        roomDBOperations();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("RecyclerView", "onScrollStateChanged");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    //chcek if it is connected to network or not ..

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    public void roomDBOperations() {

        appDatabase.userDao().UpdateSuperHerosModel();

        // this is for retriving the data from room
        List<SuperHerosModel> userdata = appDatabase.userDao().getSuperheros();
        for(int i=0;i < userdata.size();i++){
            Log.v("Superheros Data", userdata.get(i).id +" " + userdata.get(i).getName().toString());
        }

        //this is for deleting the data from room
        //  appDatabase.userDao().swipeCompleteTable();


        //this is for adding the data into room
//        for(int i=0;i < arrayListModel.size();i++){
//            String SuperHeroName = arrayListModel.get(i).getName().toString();
//            String SuperHeroRealmName = arrayListModel.get(i).getRealname().toString();
//            String SuperHeroTeamname = arrayListModel.get(i).getTeam().toString();
//            String SuperHeroImageUrl = arrayListModel.get(i).getImageurl().toString();
//            String newImageUrl = SuperHeroImageUrl.replace("https://","http://");
//
//            SuperHerosModel superHerosModel = new SuperHerosModel(SuperHeroName,SuperHeroRealmName,SuperHeroTeamname,newImageUrl);
////            SuperHerosModel superHerosModel = new SuperHerosModel("sreekar","Shabber","india","prashanth");
//            appDatabase.userDao().addSuperheros(superHerosModel);
//        }

//        UserModel userModel = new UserModel(UserName.getText().toString());
//        appDatabase.userDao().addData(userModel);
    }

    public void prepareDataSourceRecylerViewAdapter() {

        Boolean value = isNetworkConnected();

        if(value) {

            apiInterface = RetrofitApiInstance.getRetrofitInstance().create(RetrofitApiInterface.class);

            Call<ArrayList<RetrofitModel>> call = apiInterface.getAllData();

            call.enqueue(new Callback<ArrayList<RetrofitModel>>() {

                @Override
                public void onResponse(Call<ArrayList<RetrofitModel>> call, Response<ArrayList<RetrofitModel>> response) {

                    arrayListModel = response.body();

                    loadData();

                }

                @Override
                public void onFailure(Call<ArrayList<RetrofitModel>> call, Throwable t) {

                    Log.i("error", "error coocured");
                }


            });
        }else {
            loadData();
        }


       // callRecyclerView();
    }

//    public void callRecyclerView(){
//
//    }

    // this method will check weather we have an internet connection or not , if there is internet connection it will return true , else it will return false
//    private boolean isNetworkConnected() {
//        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        return cm.getActiveNetworkInfo() != null;
//    }


// load data
    public void loadData() {

        Boolean value = isNetworkConnected();

        if(value){
            friendsData1 = new ArrayList<FriendAndBestFriend>();
            for (int i = 0; i < arrayListModel.size(); i++) {
                String name = arrayListModel.get(i).getName().toString();
                String realName = arrayListModel.get(i).getRealname().toString();
                String teamName = arrayListModel.get(i).getTeam().toString();
                String friendsPhoto = arrayListModel.get(i).getImageurl().toString();
                String newFriendsPhoto = friendsPhoto.replace("https://", "http://");

                friendsData1.add(new FriendAndBestFriend(name, realName, teamName, newFriendsPhoto));
                sizeValue++;
                Log.v("data", friendsPhoto.toString());
                Log.v("data", arrayListModel.get(i).getName().toString());
                Log.v("size", Integer.toString(friendsData1.size()));
            }
        }
        else {
             friendsData1 = new ArrayList<FriendAndBestFriend>();
             List<SuperHerosModel>  superheros = appDatabase.userDao().getSuperheros();
            for (int i = 15; i < 20; i++) {
                String name = superheros.get(i).getName().toString();
                String realName = superheros.get(i).getRealName().toString();
                String teamName = superheros.get(i).getTeam().toString();
                String friendsPhoto = superheros.get(i).getSuperFriendImage().toString();
                String newFriendsPhoto = friendsPhoto.replace("https://", "http://");

                friendsData1.add(new FriendAndBestFriend(name, realName, teamName, newFriendsPhoto));
                sizeValue++;
                Log.v("data", friendsPhoto.toString());
                Log.v("data", superheros.get(i).getName().toString());
                Log.v("size", Integer.toString(friendsData1.size()));
            }
        }



        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new SwipeRecyclerViewAdapter(getApplicationContext(), friendsData1);
        ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);


       // checknetworkConnection();
         roomDBOperations();
        //sizeCondition();
        //sizeValue = friendsData1.size();
    }

//        public void checknetworkConnection() {
//            if(isNetworkConnected()){
//                Log.i("Internet","Working Fine");
//            }
//            else {
//                Log.i("Internet","Not Working Fine");
//            }
//    }

    // condition to display data , if the data that i have is zero then it should show no records else it should show records ..
    public void sizeCondition(){
        if (sizeValue == 0) {
            mRecyclerView.setVisibility(View.GONE);
            tvEmptyView.setVisibility(View.VISIBLE);

        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            tvEmptyView.setVisibility(View.GONE);
        }
    }
}
