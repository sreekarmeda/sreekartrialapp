package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.R;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by MlSr on 1/18/2018.
 */

public class MyApiMarvelDataRecyclerView extends RecyclerView.Adapter<MyApiMarvelDataRecyclerView.MarvelDataViewHolder> {

    private Context context;
    ArrayList<MyApiMarvelDataRetrofit> myApiData;
    String OldImageURL;
    String NewImageURl;

    public MyApiMarvelDataRecyclerView(Context context, ArrayList<MyApiMarvelDataRetrofit> myApiData) {
        this.context = context;
        this.myApiData = myApiData;
    }

    @Override
    public MarvelDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_marvel_data, parent, false);
        return new MarvelDataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MarvelDataViewHolder holder, int position) {

        MyApiMarvelDataRetrofit data =  myApiData.get(position);
        holder.MarveDataName.setText(data.getName().toString());
        holder.MarvelDatarealname.setText(data.getRealName().toString());
        OldImageURL = data.getImageURl();
        Log.i("ImageURL",OldImageURL);
        NewImageURl ="http://www.simplifiedcoding.net/demos/marvel/"+OldImageURL+".jpg";
        Log.i("New Image URL",NewImageURl);

        Uri myUri = Uri.parse(NewImageURl);
        Picasso.with(context).load(NewImageURl.toString()).resize(218, 192).into(holder.MarvelImageView);


//        Picasso.with(context).load(NewImageURl).into(holder.MarvelImageView);
    }

    @Override
    public int getItemCount()
    {
        return myApiData.size();
    }

    public class MarvelDataViewHolder extends RecyclerView.ViewHolder{

        private TextView MarveDataName;
        private TextView MarvelDatarealname;
        private ImageView MarvelImageView;
        public MarvelDataViewHolder(View itemView) {
            super(itemView);
            MarveDataName = (TextView) itemView.findViewById(R.id.MarvelDataName);
            MarvelDatarealname = (TextView) itemView.findViewById(R.id.marvelDataRealName);
            MarvelImageView = (ImageView) itemView.findViewById(R.id.MarvelDataImageView);

        }
    }
}
