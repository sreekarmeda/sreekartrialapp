package com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Adapters.CollectionsAdapter;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitInstance;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitInterface;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitModel;
import com.example.mlsr.analogordigitalcontinued.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MlSr on 1/22/2018.
 */

public class CollectionFragment extends Fragment {

    MyApiRetrofitInterface apiInterface;
    ArrayList<MyApiRetrofitModel> retrofitModels;
    ArrayList<MyApiMarvelDataRetrofit> myApiMarvelDataRetrofits;

    private ProgressBar collectionsFragmentProgressBar;
    private GridView collectionsFragmentGridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collection,container,false);

        collectionsFragmentProgressBar = (ProgressBar) view.findViewById(R.id.frag_coll_progress_bar);
        collectionsFragmentGridView = (GridView) view.findViewById(R.id.fragment_collection_gridView);

        apiInterface = MyApiRetrofitInstance.getRetrofitInstance().create(MyApiRetrofitInterface.class);
        Call<ArrayList<MyApiMarvelDataRetrofit>> call = apiInterface.getmarveldata();
        call.enqueue(new Callback<ArrayList<MyApiMarvelDataRetrofit>>() {
            @Override
            public void onResponse(Call<ArrayList<MyApiMarvelDataRetrofit>> call, Response<ArrayList<MyApiMarvelDataRetrofit>> response) {
                myApiMarvelDataRetrofits = response.body();
                CollectionsAdapter collectionsAdapter =  new CollectionsAdapter(getActivity(),myApiMarvelDataRetrofits);
                collectionsFragmentGridView.setAdapter(collectionsAdapter);

            }

            @Override
            public void onFailure(Call<ArrayList<MyApiMarvelDataRetrofit>> call, Throwable t) {

            }
        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
