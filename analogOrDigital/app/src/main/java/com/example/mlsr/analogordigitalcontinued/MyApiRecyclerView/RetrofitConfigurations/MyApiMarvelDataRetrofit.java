package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by MlSr on 1/18/2018.
 */
@SuppressWarnings("serial")
public class MyApiMarvelDataRetrofit  implements Serializable {
    @SerializedName("Bio")
    @Expose
    private String bio;
    @SerializedName("CreatedBy")
    @Expose
    private String createdBy;
    @SerializedName("FirstAppearence")
    @Expose
    private String firstAppearence;
    @SerializedName("ImageURl")
    @Expose
    private String imageURl;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("RealName")
    @Expose
    private String realName;
    @SerializedName("Team")
    @Expose
    private String team;

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getFirstAppearence() {
        return firstAppearence;
    }

    public void setFirstAppearence(String firstAppearence) {
        this.firstAppearence = firstAppearence;
    }

    public String getImageURl() {
        return imageURl;
    }

    public void setImageURl(String imageURl) {
        this.imageURl = imageURl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }
}
