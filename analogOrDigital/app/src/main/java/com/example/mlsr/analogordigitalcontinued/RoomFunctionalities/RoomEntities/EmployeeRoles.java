package com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by MlSr on 1/10/2018.
 */

@Entity
public class EmployeeRoles {

    private int RoleID;

    @PrimaryKey
    @NonNull
    private String RoleName;

    public EmployeeRoles(int roleID, String roleName) {
        RoleID = roleID;
        RoleName = roleName;
    }

    public EmployeeRoles() {
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }
}
