package com.example.mlsr.analogordigitalcontinued;

import java.io.Serializable;

/**
 * Created by MlSr on 1/2/2018.
 */

public class FriendAndBestFriend {


    private String FriendName;
    private String BestFriendName;
    private String FriendsTeam;
    private String FriendsPhoto;

    public FriendAndBestFriend(String friendName, String bestFriendName, String friendsTeam, String friendsPhoto) {
        FriendName = friendName;
        BestFriendName = bestFriendName;
        FriendsTeam = friendsTeam;
        FriendsPhoto = friendsPhoto;
    }

    public FriendAndBestFriend() {
    }

    public String getFriendName() {
        return FriendName;
    }

    public void setFriendName(String friendName) {
        FriendName = friendName;
    }

    public String getBestFriendName() {
        return BestFriendName;
    }

    public void setBestFriendName(String bestFriendName) {
        BestFriendName = bestFriendName;
    }

    public String getFriendsTeam() {
        return FriendsTeam;
    }

    public void setFriendsTeam(String friendsTeam) {
        FriendsTeam = friendsTeam;
    }

    public String getFriendsPhoto() {
        return FriendsPhoto;
    }

    public void setFriendsPhoto(String friendsPhoto) {
        FriendsPhoto = friendsPhoto;
    }

}
