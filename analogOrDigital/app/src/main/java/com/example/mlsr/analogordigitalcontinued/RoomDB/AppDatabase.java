package com.example.mlsr.analogordigitalcontinued.RoomDB;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities.EmployeeLoginDetails;
import com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities.EmployeeRoles;

/**
 * Created by MlSr on 1/5/2018.
 */
@Database(entities = {UserModel.class,SuperHerosModel.class, EmployeeRoles.class, EmployeeLoginDetails.class},version = 10)
public abstract class AppDatabase extends RoomDatabase{
    private static AppDatabase INSTANCE;


//    static final Migration MIGRATION_1_8 = new Migration(1, 8) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//
//        }
//    };

    public static AppDatabase getDatabase(Context context){
        if(INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"user_DB").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public abstract UserModelDao userDao();

}
