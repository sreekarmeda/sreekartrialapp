package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;


import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView.MyApiMarvelDataRecyclerView;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView.MyApiMarvelDataRecyclerViewActivity;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView.MyApiRecyclerView;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.Employee;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.EmployeeGson;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitInstance;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitInterface;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitModel;
import com.example.mlsr.analogordigitalcontinued.R;
import com.example.mlsr.analogordigitalcontinued.RetrofitApiInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyApimainActivity extends AppCompatActivity {

    private Button GetAllDataButton;
    private RecyclerView myRecyclerView;
    private Button SendDataButton;
    private Button ButtonToSeeMarvelInfo;
    MyApiRecyclerView myApiAdapterRecyclerView;
    Employee EmpData ;

    MyApiRetrofitInterface apiInterface;
    ArrayList<MyApiRetrofitModel> retrofitModels;
    ArrayList<MyApiMarvelDataRetrofit> myApiMarvelDataRetrofits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_apimain);
        GetAllDataButton = (Button) findViewById(R.id.GetDataButton);
        SendDataButton = (Button) findViewById(R.id.SendDataButton);
        myRecyclerView = (RecyclerView) findViewById(R.id.MyApiRecyclerView);
        ButtonToSeeMarvelInfo = (Button) findViewById(R.id.ButtonToSeeMarvelData);


        getAllData();
        sendDataToApi();
        getMarvelData();

    }

    public void getMarvelData(){
        ButtonToSeeMarvelInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                apiInterface = MyApiRetrofitInstance.getRetrofitInstance().create(MyApiRetrofitInterface.class);
                Call<ArrayList<MyApiMarvelDataRetrofit>> call = apiInterface.getmarveldata();
                call.enqueue(new Callback<ArrayList<MyApiMarvelDataRetrofit>>() {
                    @Override
                    public void onResponse(Call<ArrayList<MyApiMarvelDataRetrofit>> call, Response<ArrayList<MyApiMarvelDataRetrofit>> response) {
                        Log.i("response",response.toString());
                        myApiMarvelDataRetrofits = response.body();
                        Intent i = new Intent(MyApimainActivity.this, MyApiMarvelDataRecyclerViewActivity.class);
                        i.putExtra("TestData",myApiMarvelDataRetrofits);
                      //i.putStringArrayListExtra("stock_list", myApiMarvelDataRetrofits);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<MyApiMarvelDataRetrofit>> call, Throwable t) {

                    }
                });
            }
        });
    }

    public void sendDataToApi(){
    SendDataButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EmployeeGson employeeGson = new EmployeeGson();
            employeeGson.setName("vasavi");
            employeeGson.setAge(24);
            EmpData = new Employee();
            EmpData.setAge(25);
            EmpData.setName("Sreekar");

            apiInterface = MyApiRetrofitInstance.getRetrofitInstance().create(MyApiRetrofitInterface.class);
            Call<Integer> call = apiInterface.Addnames(employeeGson);
            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    Integer test = response.body();
                    Log.i("RTest",test.toString());
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {

                    Log.e("er",t.getMessage());
                }
            });
        }
    });
    }


    public void getAllData(){
        GetAllDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                apiInterface = MyApiRetrofitInstance.getRetrofitInstance().create(MyApiRetrofitInterface.class);
                Call<ArrayList<MyApiRetrofitModel>> call = apiInterface.getAllNames();
                call.enqueue(new Callback<ArrayList<MyApiRetrofitModel>>() {
                    @Override
                    public void onResponse(Call<ArrayList<MyApiRetrofitModel>> call, Response<ArrayList<MyApiRetrofitModel>> response) {
                        retrofitModels = response.body();
                        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myRecyclerView.setHasFixedSize(true);
                        myApiAdapterRecyclerView = new MyApiRecyclerView(retrofitModels,getApplicationContext());
                        myRecyclerView.setAdapter(myApiAdapterRecyclerView);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<MyApiRetrofitModel>> call, Throwable t) {

                    }
                });

            }
        });
    }
}
