package com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.example.mlsr.analogordigitalcontinued.R;

/**
 * Created by MlSr on 1/22/2018.
 */

public class Function {

    public static void changeMainFragment(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_FrameLayout,fragment)
                .commit();
    }
}
