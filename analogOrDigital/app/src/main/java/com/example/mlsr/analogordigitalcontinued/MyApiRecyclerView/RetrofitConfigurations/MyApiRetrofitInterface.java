package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by MlSr on 1/17/2018.
 */

public interface MyApiRetrofitInterface {

    @GET("GetAllNames")
    Call<ArrayList<MyApiRetrofitModel>> getAllNames();

    @POST("Addnames")
    Call<Integer> Addnames(@Body EmployeeGson empData);

    @GET("getMarvelData")
    Call<ArrayList<MyApiMarvelDataRetrofit>> getmarveldata();

}
