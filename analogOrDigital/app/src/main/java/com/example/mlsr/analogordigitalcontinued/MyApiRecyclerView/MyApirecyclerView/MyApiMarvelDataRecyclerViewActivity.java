package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitModel;
import com.example.mlsr.analogordigitalcontinued.R;

import java.util.ArrayList;

public class MyApiMarvelDataRecyclerViewActivity extends AppCompatActivity {

    ArrayList<MyApiMarvelDataRetrofit> stock_list;
    MyApiMarvelDataRecyclerView myApiMarvelDataRecyclerView ;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_api_marvel_data_recycler_view);

        recyclerView = (RecyclerView) findViewById(R.id.marvelDataRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        Intent i = getIntent();
        stock_list =  (ArrayList<MyApiMarvelDataRetrofit>) i.getSerializableExtra("TestData");
        myApiMarvelDataRecyclerView = new MyApiMarvelDataRecyclerView(getApplicationContext(),stock_list);

       recyclerView.setAdapter(myApiMarvelDataRecyclerView);

//         ArrayList<MyApiMarvelDataRetrofit> data =(ArrayList<MyApiMarvelDataRetrofit>) i.getSerializableExtra("stock_list");

      Log.i("data values",stock_list.get(0).getName());
        Log.i("data valeus1",stock_list.get(0).getImageURl());

    }
}
