package com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Adapters.PhotosAdapter;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitInstance;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitInterface;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitModel;
import com.example.mlsr.analogordigitalcontinued.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MlSr on 1/22/2018.
 */

public class PhotosFragment extends Fragment {

    MyApiRetrofitInterface apiInterface;
    ArrayList<MyApiRetrofitModel> retrofitModels;
    ArrayList<MyApiMarvelDataRetrofit> myApiMarvelDataRetrofits;

    private RecyclerView photosRecyclerView;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_photos,container,false);

       progressBar = (ProgressBar) view.findViewById(R.id.fragment_photos_progressBar);
       photosRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_photos_recyclerView);

        apiInterface = MyApiRetrofitInstance.getRetrofitInstance().create(MyApiRetrofitInterface.class);
        Call<ArrayList<MyApiMarvelDataRetrofit>> call = apiInterface.getmarveldata();
        call.enqueue(new Callback<ArrayList<MyApiMarvelDataRetrofit>>() {
            @Override
            public void onResponse(Call<ArrayList<MyApiMarvelDataRetrofit>> call, Response<ArrayList<MyApiMarvelDataRetrofit>> response) {
                myApiMarvelDataRetrofits = response.body();

                photosRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                photosRecyclerView.setHasFixedSize(true);
                PhotosAdapter photosAdapter = new PhotosAdapter(getActivity(),myApiMarvelDataRetrofits);
                photosRecyclerView.setAdapter(photosAdapter);

            }

            @Override
            public void onFailure(Call<ArrayList<MyApiMarvelDataRetrofit>> call, Throwable t) {

            }
        });



       return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
