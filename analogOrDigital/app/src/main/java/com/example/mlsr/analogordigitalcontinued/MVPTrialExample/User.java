package com.example.mlsr.analogordigitalcontinued.MVPTrialExample;

/**
 * Created by MlSr on 2/1/2018.
 */

public class User {
    private String id;
    private String first_Name;
    private String last_Name;

    public User(String id, String first_Name, String last_Name) {
        this.id = id;
        this.first_Name = first_Name;
        this.last_Name = last_Name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_Name() {
        return first_Name;
    }

    public void setFirst_Name(String first_Name) {
        this.first_Name = first_Name;
    }

    public String getLast_Name() {
        return last_Name;
    }

    public void setLast_Name(String last_Name) {
        this.last_Name = last_Name;
    }
}
