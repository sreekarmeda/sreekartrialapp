package com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView.MyApiMarvelDataRecyclerView;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by MlSr on 1/23/2018.
 */

public class CollectionsAdapter extends BaseAdapter {
    private Context context;
    private List<MyApiMarvelDataRetrofit> collections;

    public CollectionsAdapter(Context context, List<MyApiMarvelDataRetrofit> collections) {
        this.context = context;
        this.collections = collections;
    }

    @Override
    public int getCount() {
        return collections.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.item_collection,viewGroup,false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        MyApiMarvelDataRetrofit data = collections.get(i);
        holder.title.setText(data.getName());
        holder.totalPhotos.setText(Integer.toString(collections.size()));

        return view;
    }

    static class ViewHolder{
        private TextView title;
        private TextView totalPhotos;
        private ImageView Collection_square_image;


        public ViewHolder(View view) {
            title = (TextView) view.findViewById(R.id.item_collection_title);
            totalPhotos = (TextView) view.findViewById(R.id.item_collection_photos_count);
            Collection_square_image = (ImageView) view.findViewById(R.id.item_collection_photo);
        }
    }
}
