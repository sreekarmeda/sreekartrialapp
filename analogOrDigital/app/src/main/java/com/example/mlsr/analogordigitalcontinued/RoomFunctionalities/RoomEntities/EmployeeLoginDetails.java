package com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by MlSr on 1/10/2018.
 */

@Entity(foreignKeys = {
        @ForeignKey(
                entity = EmployeeRoles.class,
                parentColumns = "RoleName",
                childColumns = "Rolename"
        )
        })

public class EmployeeLoginDetails {

    @PrimaryKey
    @NonNull
    private String UserName;
    private String Password;
    private String Rolename;

    public EmployeeLoginDetails(String userName, String password, String rolename) {
        UserName = userName;
        Password = password;
        Rolename = rolename;
    }

    public EmployeeLoginDetails() {

    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getRolename() {
        return Rolename;
    }

    public void setRolename(String rolename) {
        Rolename = rolename;
    }
}
