package com.example.mlsr.analogordigitalcontinued.RoomDB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by MlSr on 1/5/2018.
 */
@Entity
public class UserModel {
    @PrimaryKey(autoGenerate = true)
    public int id;

    private String name;

    public UserModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
