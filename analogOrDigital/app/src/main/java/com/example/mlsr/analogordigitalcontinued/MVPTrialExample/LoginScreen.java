package com.example.mlsr.analogordigitalcontinued.MVPTrialExample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mlsr.analogordigitalcontinued.MainActivity;
import com.example.mlsr.analogordigitalcontinued.R;

public class LoginScreen extends AppCompatActivity implements LoginScreenMVP.View{


    private EditText Username_EditText;
    private EditText Password_EditText;
    private Button LoginButton;

    LoginScreenMVP.Presenter presenter;

    String password;
    String username;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        presenter = new LoginScreenPresenter(this);

        Username_EditText = (EditText) findViewById(R.id.login_screen_username);
        Password_EditText = (EditText) findViewById(R.id.loginscreen_password);
        LoginButton = (Button) findViewById(R.id.LoginScreen_Button);

        onLoginButtonClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onLoginButtonClick(){
        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.getTestData();
            }
        });
    }

    @Override
    public String getUserName() {
        username = Username_EditText.getText().toString();
        return username ;
    }

    @Override
    public String getPassWord() {
        password = Password_EditText.getText().toString();
        return password;
    }

    @Override
    public void setUserName(String name) {
        Username_EditText.setText(name);
    }

    @Override
    public void setPassWord(String passWord) {
        Password_EditText.setText(passWord);
    }
}
