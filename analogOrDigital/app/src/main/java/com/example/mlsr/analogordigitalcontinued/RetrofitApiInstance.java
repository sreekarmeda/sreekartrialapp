package com.example.mlsr.analogordigitalcontinued;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Win on 22-11-2017.
 */

public class RetrofitApiInstance {
    public static final String base_url = "http://simplifiedcoding.net/";
    public static Retrofit retrofit = null;

    public static Retrofit getRetrofitInstance(){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(base_url).addConverterFactory(GsonConverterFactory.create()).build();
            return retrofit;
        }
        return retrofit;
    }
}
