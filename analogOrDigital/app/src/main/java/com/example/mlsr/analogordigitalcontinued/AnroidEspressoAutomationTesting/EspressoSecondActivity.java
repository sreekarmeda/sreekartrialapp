package com.example.mlsr.analogordigitalcontinued.AnroidEspressoAutomationTesting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.widget.TextView;

import com.example.mlsr.analogordigitalcontinued.R;

public class EspressoSecondActivity extends AppCompatActivity {

    private TextView DisplaytextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espresso_second);

        DisplaytextView = (TextView) findViewById(R.id.resultView);

        Bundle inputData = getIntent().getExtras();
        String input = inputData.getString("input");
        DisplaytextView.setText(input);
    }
}
