package com.example.mlsr.analogordigitalcontinued;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Win on 22-11-2017.
 */

public interface RetrofitApiInterface {

    @GET("demos/marvel/")
    Call<ArrayList<RetrofitModel>> getAllData();

}
