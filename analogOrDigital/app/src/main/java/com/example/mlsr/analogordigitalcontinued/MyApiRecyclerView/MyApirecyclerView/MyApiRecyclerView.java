package com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.MyApirecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiRetrofitModel;
import com.example.mlsr.analogordigitalcontinued.R;

import java.util.ArrayList;

/**
 * Created by MlSr on 1/17/2018.
 */

public class MyApiRecyclerView extends RecyclerView.Adapter<MyApiRecyclerView.NamesDataViewHolder>{

    private Context context;
    ArrayList<MyApiRetrofitModel> myApiRetrofitModels;

    public MyApiRecyclerView(ArrayList<MyApiRetrofitModel> myApiRetrofitModels,Context context) {
        this.context = context;
        this.myApiRetrofitModels = myApiRetrofitModels;
    }

    @Override
    public NamesDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new NamesDataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NamesDataViewHolder holder, int position) {
        MyApiRetrofitModel data = myApiRetrofitModels.get(position);


        holder.textView.setText(data.getName().toString());

    }

    @Override
    public int getItemCount() {
        return myApiRetrofitModels.size();
    }

    public class NamesDataViewHolder extends RecyclerView.ViewHolder{

        private TextView textView;

        public NamesDataViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textViewName);
        }
    }
}
