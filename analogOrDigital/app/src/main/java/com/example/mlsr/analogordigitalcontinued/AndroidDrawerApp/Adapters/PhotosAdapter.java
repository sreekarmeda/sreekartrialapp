package com.example.mlsr.analogordigitalcontinued.AndroidDrawerApp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mlsr.analogordigitalcontinued.MyApiRecyclerView.RetrofitConfigurations.MyApiMarvelDataRetrofit;
import com.example.mlsr.analogordigitalcontinued.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by MlSr on 1/23/2018.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder> {

    private Context context;
    ArrayList<MyApiMarvelDataRetrofit> myApiData;
    String OldImageURL;
    String NewImageURl;

    public PhotosAdapter(Context context, ArrayList<MyApiMarvelDataRetrofit> myApiData) {
        this.context = context;
        this.myApiData = myApiData;
    }

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photos, parent, false);
        return new PhotosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PhotosViewHolder holder, int position) {

        MyApiMarvelDataRetrofit data = myApiData.get(position);

        holder.imagename.setText(data.getName().toString());

        OldImageURL = data.getImageURl();
        Log.i("ImageURL",OldImageURL);
        NewImageURl ="http://www.simplifiedcoding.net/demos/marvel/"+OldImageURL+".jpg";
        Log.i("New Image URL",NewImageURl);
        Picasso.with(context).load(NewImageURl.toString()).into(holder.fullImage);



    }

    @Override
    public int getItemCount() {
        return myApiData.size();
    }

    public class PhotosViewHolder extends RecyclerView.ViewHolder{

        private ImageView fullImage;
        private ImageView circularImage;
        private TextView imagename;

        public PhotosViewHolder(View itemView) {
            super(itemView);
            fullImage = (ImageView) itemView.findViewById(R.id.item_photo_photo);
            circularImage = (ImageView) itemView.findViewById(R.id.item_photo_user_avatar);
            imagename = (TextView) itemView.findViewById(R.id.Image_Name_Avatar);
        }
    }
}
