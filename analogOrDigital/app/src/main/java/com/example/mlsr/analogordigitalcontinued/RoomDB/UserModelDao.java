package com.example.mlsr.analogordigitalcontinued.RoomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities.EmployeeLoginDetails;
import com.example.mlsr.analogordigitalcontinued.RoomFunctionalities.RoomEntities.EmployeeRoles;
import java.util.List;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by MlSr on 1/5/2018.
 */

@Dao
public interface UserModelDao {
    @Query("select * from UserModel")
    List<UserModel> getUser();

    @Insert (onConflict = REPLACE)
    void addData(UserModel userMode);

    @Delete
    void deleteData(UserModel user);


    @Insert (onConflict = REPLACE)
    void addSuperheros(SuperHerosModel SuperHerosmodel);

    @Query("select * from SuperHerosModel")
    List<SuperHerosModel> getSuperheros();

    @Query("DELETE FROM SuperHerosModel")
    public void swipeCompleteTable();

    @Query("Update SuperHerosModel SET name = 'shabber' where name = 'sreekar' ")
    public  void UpdateSuperHerosModel();

    @Insert(onConflict = REPLACE)
    void addEmployeeRoles(EmployeeRoles employeeRoles);

    @Insert(onConflict = REPLACE)
    void addEmployeeLoginDetails(EmployeeLoginDetails employeeLoginDetails);

    @Query("select * from EmployeeLoginDetails")
    List<EmployeeLoginDetails> getAllEmployeeLoginDetails();

    @Query("select * from EmployeeRoles")
    List<EmployeeRoles> getAllEmployeeRoles();

}
