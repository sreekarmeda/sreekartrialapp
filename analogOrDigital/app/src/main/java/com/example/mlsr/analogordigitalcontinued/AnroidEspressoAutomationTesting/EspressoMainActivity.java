package com.example.mlsr.analogordigitalcontinued.AnroidEspressoAutomationTesting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mlsr.analogordigitalcontinued.R;

public class EspressoMainActivity extends AppCompatActivity {

    private EditText EditTextView;
    private Button firstButton ;
    private Button SecondButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_espresso_main);

        EditTextView = (EditText) findViewById(R.id.inputField);

        firstButton = (Button) findViewById(R.id.changeText);
        SecondButton = (Button) findViewById(R.id.switchActivity);

        firstButtonClick();
        secondButtonClick();

    }

    public void firstButtonClick() {
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTextView.setText("Hello world");
            }
        });

    }

    public void secondButtonClick(){

        SecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EspressoMainActivity.this, EspressoSecondActivity.class);
                intent.putExtra("input", EditTextView.getText().toString());
                startActivity(intent);
            }
        });

    }
}
