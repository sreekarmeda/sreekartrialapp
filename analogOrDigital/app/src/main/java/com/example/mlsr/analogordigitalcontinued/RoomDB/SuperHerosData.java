package com.example.mlsr.analogordigitalcontinued.RoomDB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by MlSr on 1/8/2018.
 */
@Entity
public class SuperHerosData {

    @PrimaryKey(autoGenerate = true)
    public int id;

    private String name ;

    private String realName;

    private String team;

    private String superFriendImage;

    public SuperHerosData(String name, String realName, String team, String superFriendImage) {
        this.name = name;
        this.realName = realName;
        this.team = team;
        this.superFriendImage = superFriendImage;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getSuperFriendImage() {
        return superFriendImage;
    }

    public void setSuperFriendImage(String superFriendImage) {
        this.superFriendImage = superFriendImage;
    }
}
